import os
import sys
import time


def countdown(time_sec):
    while time_sec:
        os.system("tput clear")
        mins, secs = divmod(time_sec, 60)
        timeformat = '{:02d}:{:02d}'.format(mins, secs)
        print("Time till acttiviation:".center(os.get_terminal_size().columns))
        print("\n ".center(os.get_terminal_size().columns) + timeformat, end='\r')
        time.sleep(1)
        time_sec -= 1


args = int(sys.argv[1])
countdown(args)
