read -p "
Enter the time recording should start: (format 24h- HH:MM:SS). Example: 17:29:00...

" time


hour=$(echo ${time} | awk -F":" '{print $1}' | awk '{sub(/^0*/,"");}1')
min=$(echo ${time} | awk -F":" '{print $2}' | awk '{sub(/^0*/,"");}1')
sec=$(echo ${time} | awk -F":" '{print $3}' | awk '{sub(/^0*/,"");}1')


converHour=$(echo $(( ${hour} * 3600 )))

if [[ ${min} -ne "0" ]]; then
  converMin=$(echo $(( ${min} * 60 )))
else
  converMin="0"
fi
if [[ ${sec} -ne "0" ]]; then
  secs=$(echo ${sec})
else
  secs="0"
fi

totalSecs=$(echo $(( ${converMin} + ${converHour} + ${secs} )))
currentH=$(echo $(( $(date +%H | awk '{sub(/^0*/,"");}1') * 3600 )))
currentM=$(echo $(( $(date +%M | awk '{sub(/^0*/,"");}1') * 60 )))
currentSecs=$(echo $(( ${currentH} + ${currentM} + $(date +%S | awk '{sub(/^0*/,"");}1') )))

math=$(echo $((  ${totalSecs} - ${currentSecs}  - 1 )))

python $(locate countDown.py) "${math}" &&
RED='\033[0;31m'
NC='\033[0m' # No Color
printf "${RED}ADD SOME COMMAND HERE!!!!!${NC}\n"
